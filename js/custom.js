// $(function(){
//
// 	var hf = function(){
// 		var h_header = $('header').height();
// 		var h_footer = $('footer').height();
// 		$('.content').css({
// 			'paddingTop': h_header,
// 			'paddingBottom': h_footer
// 		});
// 	}
//
// 	$(window).bind('load resize', hf);
// });

$(function(){
	$('.menu-toggle').click(function(){
		$(this).toggleClass('active')
		$('.menu').slideToggle(700)
	});
$('.tabs a').click(function(){
	$(this).parents('.tab-wrap').find('.tab-cont').addClass('hide');
	$(this).parent().siblings().removeClass('active');
	var id = $(this).attr('href');
	$(id).removeClass('hide');
	$(this).parent().addClass('active');
	return false
});
	// $('.banner-slider, .testimonial-slider').slick({
	// 	arrows: false,
	// 	dots: true,
	// });
	$('.quote-content-slider-slide').slick({
		dots: false,
		// arrows:false,
		appendArrows:'.quote-content-slider-slide-buttons',
		prevArrow: '<button type="button" class="slick-prev"></button>',
		nextArrow: '<button type="button" class="slick-next"></button>',
		responsive: [
			{
				breakpoint: 767,
				settings: {
					dots: false,
				}
			}
		]
	});
});
